.. kosmos-specs documentation master file, created by
   sphinx-quickstart on Tue Jul  9 22:26:36 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

kosmos-specs Design Specifications
==================================================

Liberty approved specs:

.. toctree::
   :glob:
   :maxdepth: 1

   specs/liberty/*

Mitaka approved specs:

.. toctree::
   :glob:
   :maxdepth: 1

   specs/mitaka/*


kosmos-specs Repository Information
===================================================

.. toctree::
   :maxdepth: 2

   README <readme>
   contributing


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
